package org.idx.scheduler.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.idx.scheduler.dob.Schedule;
import org.idx.scheduler.dob.ScheduledJobDOB;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public interface SchedulerService {


	boolean scheduleJob(String jobName, Schedule schedule);
	boolean scheduleOneTimeJob(String jobName, Class<? extends QuartzJobBean> jobClass, Date date);
	boolean scheduleCronJob(String jobName, Class<? extends QuartzJobBean> jobClass, Date date, String cronExpression);
	boolean updateOneTimeJob(String jobName, Date date);
	boolean updateCronJob(String jobName, Date date, String cronExpression);
	boolean unScheduleJob(String jobName);
	boolean deleteJob(String jobName);
	boolean pauseJob(String jobName);
	boolean resumeJob(String jobName);
	boolean startJobNow(String jobName);
	boolean isJobRunning(String jobName);
	List<Map<String, Object>> getAllJobs();
	boolean isJobWithNamePresent(String jobName);
	String getJobState(String jobName, String jobGroup);
	boolean stopJob(String jobName);
	boolean createJob(ScheduledJobDOB job);
	ScheduledJobDOB getJobDetails(String jobName) throws SchedulerException;


}
