package org.idx.scheduler;

import java.util.Date;

import org.idx.scheduler.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

@Component
public class XSchedulerInitializer implements CommandLineRunner {

	@Autowired
	SchedulerFactoryBean schedulerFactoryBean;

	@Autowired
	SchedulerService schedulerService;

	public void run(String... args) throws Exception {
		System.out.println("###################### XScheduler Init Called ########################");
	}

}
