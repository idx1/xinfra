package org.idx.scheduler.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class SchedulerRestTest {

	public static void main(String args[]) {
		System.out.println("############ SchedulerRestTest ##########");
		
		
		Client client = ClientBuilder.newClient();
		
		WebTarget webTarget 
		  = client.target("http://localhost:7080/scheduler");
		
		WebTarget scheduleWebTarget = webTarget.path("schedule");
		
		Invocation.Builder invocationBuilder 
		  = scheduleWebTarget.request(MediaType.APPLICATION_JSON);
		
		scheduleWebTarget
        .request(MediaType.APPLICATION_JSON)
        .get(abc.class);		
	}
	
	
	class abc {
		
		private String jobName="";
		private String jobScheduleTime="";
		private String cronExpression= "";
		public String getJobName() {
			return jobName;
		}
		public void setJobName(String jobName) {
			this.jobName = jobName;
		}
		public String getJobScheduleTime() {
			return jobScheduleTime;
		}
		public void setJobScheduleTime(String jobScheduleTime) {
			this.jobScheduleTime = jobScheduleTime;
		}
		public String getCronExpression() {
			return cronExpression;
		}
		public void setCronExpression(String cronExpression) {
			this.cronExpression = cronExpression;
		}
		
		
		
		
	}
	
}
