package org.idx.scheduler.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.idx.scheduler.dob.JOB_REPEATINTERVAL_TYPE;
import org.idx.scheduler.dob.Schedule;
import org.idx.scheduler.dob.ScheduledJobDOB;
import org.idx.scheduler.utils.SchedulerUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.expression.ParseException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Service;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.SimpleScheduleBuilder.*;

@Service
public class SchedulerServiceImpl implements SchedulerService{

	@Autowired
	@Lazy
	SchedulerFactoryBean schedulerFactoryBean;

	@Autowired
	private ApplicationContext context;

	public boolean scheduleJob(String jobName, Schedule schedule) {
 		return false;
	}

	public boolean createJob(ScheduledJobDOB job) {

		try {
			JOB_REPEATINTERVAL_TYPE intervalType = job.getIntervalType();
			int interval = job.getRepeatInterval();

			String jobKey = job.getName();
			String groupKey = "IDXSchdulerGroup";
			String triggerKey = job.getName();

			Class<? extends QuartzJobBean> jobClass = (Class<? extends QuartzJobBean>) Class.forName(job.getClassName());

			JobDetail jobDetail = SchedulerUtils.createJob(jobClass, false, context, jobKey, groupKey);

			if(intervalType.equals(JOB_REPEATINTERVAL_TYPE.MINUTES)) {
				interval = job.getRepeatInterval()*60;
			}
			else			if(intervalType.equals(JOB_REPEATINTERVAL_TYPE.HOURS)) {
				interval = job.getRepeatInterval()*60*60;
			}
			if(intervalType.equals(JOB_REPEATINTERVAL_TYPE.DAYS)) {
				interval = job.getRepeatInterval()*60*60*24;
			}

			System.out.println("interval : "+ interval);

			SimpleTrigger trigger = null;
			if(intervalType.equals(JOB_REPEATINTERVAL_TYPE.MINUTES)) {

				SimpleTriggerFactoryBean bean = new SimpleTriggerFactoryBean();
				bean.setName(job.getName());
				bean.setRepeatInterval(interval*60);
				bean.setStartTime(job.getStartDate());


			    try {
			        bean.afterPropertiesSet();
			    } catch (ParseException e) {
			        e.printStackTrace();
			    }

			    trigger =  bean.getObject();
			}

			Scheduler scheduler = schedulerFactoryBean.getScheduler();

			System.out.println(trigger);

			scheduler.scheduleJob(jobDetail, trigger);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}


	/**
	 * Schedule a job by jobName at given date.
	 */
	public boolean scheduleOneTimeJob(String jobName, Class<? extends QuartzJobBean> jobClass, Date date) {
		System.out.println("Request received to scheduleJob");

		String jobKey = jobName;
		String groupKey = "SampleGroup";
		String triggerKey = jobName;

		JobDetail jobDetail = SchedulerUtils.createJob(jobClass, false, context, jobKey, groupKey);

		System.out.println("creating trigger for key :"+jobKey + " at date :"+date);
		Trigger cronTriggerBean = SchedulerUtils.createSingleTrigger(triggerKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
		//Trigger cronTriggerBean = JobUtil.createSingleTrigger(triggerKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);

		try {
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			Date dt = scheduler.scheduleJob(jobDetail, cronTriggerBean);
			System.out.println("Job with key jobKey :"+jobKey+ " and group :"+groupKey+ " scheduled successfully for date :"+dt);
			return true;
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while scheduling job with key :"+jobKey + " message :"+e.getMessage());
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Schedule a job by jobName at given date.
	 */
	public boolean scheduleCronJob(String jobName, Class<? extends QuartzJobBean> jobClass, Date date, String cronExpression) {
		System.out.println("Request received to scheduleJob");

		String jobKey = jobName;
		String groupKey = "SampleGroup";
		String triggerKey = jobName;

		JobDetail jobDetail = SchedulerUtils.createJob(jobClass, false, context, jobKey, groupKey);

		System.out.println("creating trigger for key :"+jobKey + " at date :"+date);
		Trigger cronTriggerBean = SchedulerUtils.createCronTrigger(triggerKey, date, cronExpression, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

		try {
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			Date dt = scheduler.scheduleJob(jobDetail, cronTriggerBean);
			System.out.println("Job with key jobKey :"+jobKey+ " and group :"+groupKey+ " scheduled successfully for date :"+dt);
			return true;
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while scheduling job with key :"+jobKey + " message :"+e.getMessage());
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Update one time scheduled job.
	 */
	public boolean updateOneTimeJob(String jobName, Date date) {
		System.out.println("Request received for updating one time job.");

		String jobKey = jobName;

		System.out.println("Parameters received for updating one time job : jobKey :"+jobKey + ", date: "+date);
		try {
			//Trigger newTrigger = JobUtil.createSingleTrigger(jobKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
			Trigger newTrigger = SchedulerUtils.createSingleTrigger(jobKey, date, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

			Date dt = schedulerFactoryBean.getScheduler().rescheduleJob(TriggerKey.triggerKey(jobKey), newTrigger);
			System.out.println("Trigger associated with jobKey :"+jobKey+ " rescheduled successfully for date :"+dt);
			return true;
		} catch ( Exception e ) {
			System.out.println("SchedulerException while updating one time job with key :"+jobKey + " message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Update scheduled cron job.
	 */
	public boolean updateCronJob(String jobName, Date date, String cronExpression) {
		System.out.println("Request received for updating cron job.");

		String jobKey = jobName;

		System.out.println("Parameters received for updating cron job : jobKey :"+jobKey + ", date: "+date);
		try {
			Trigger newTrigger = SchedulerUtils.createCronTrigger(jobKey, date, cronExpression, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

			Date dt = schedulerFactoryBean.getScheduler().rescheduleJob(TriggerKey.triggerKey(jobKey), newTrigger);
			System.out.println("Trigger associated with jobKey :"+jobKey+ " rescheduled successfully for date :"+dt);
			return true;
		} catch ( Exception e ) {
			System.out.println("SchedulerException while updating cron job with key :"+jobKey + " message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Remove the indicated Trigger from the scheduler.
	 * If the related job does not have any other triggers, and the job is not durable, then the job will also be deleted.
	 */
	public boolean unScheduleJob(String jobName) {
		System.out.println("Request received for Unscheduleding job.");

		String jobKey = jobName;

		TriggerKey tkey = new TriggerKey(jobKey);
		System.out.println("Parameters received for unscheduling job : tkey :"+jobKey);
		try {
			boolean status = schedulerFactoryBean.getScheduler().unscheduleJob(tkey);
			System.out.println("Trigger associated with jobKey :"+jobKey+ " unscheduled with status :"+status);
			return status;
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while unscheduling job with key :"+jobKey + " message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Delete the identified Job from the Scheduler - and any associated Triggers.
	 */
	public boolean deleteJob(String jobName) {
		System.out.println("Request received for deleting job.");

		String jobKey = jobName;
		String groupKey = "SampleGroup";

		JobKey jkey = new JobKey(jobKey, groupKey);
		System.out.println("Parameters received for deleting job : jobKey :"+jobKey);

		try {
			boolean status = schedulerFactoryBean.getScheduler().deleteJob(jkey);
			System.out.println("Job with jobKey :"+jobKey+ " deleted with status :"+status);
			return status;
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while deleting job with key :"+jobKey + " message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Pause a job
	 */
	public boolean pauseJob(String jobName) {
		System.out.println("Request received for pausing job.");

		String jobKey = jobName;
		String groupKey = "SampleGroup";
		JobKey jkey = new JobKey(jobKey, groupKey);
		System.out.println("Parameters received for pausing job : jobKey :"+jobKey+ ", groupKey :"+groupKey);

		try {
			schedulerFactoryBean.getScheduler().pauseJob(jkey);
			System.out.println("Job with jobKey :"+jobKey+ " paused succesfully.");
			return true;
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while pausing job with key :"+jobName + " message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Resume paused job
	 */
	public boolean resumeJob(String jobName) {
		System.out.println("Request received for resuming job.");

		String jobKey = jobName;
		String groupKey = "SampleGroup";

		JobKey jKey = new JobKey(jobKey, groupKey);
		System.out.println("Parameters received for resuming job : jobKey :"+jobKey);
		try {
			schedulerFactoryBean.getScheduler().resumeJob(jKey);
			System.out.println("Job with jobKey :"+jobKey+ " resumed succesfully.");
			return true;
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while resuming job with key :"+jobKey+ " message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Start a job now
	 */
	public boolean startJobNow(String jobName) {
		System.out.println("Request received for starting job now.");

		String jobKey = jobName;
		String groupKey = "SampleGroup";

		JobKey jKey = new JobKey(jobKey, groupKey);
		System.out.println("Parameters received for starting job now : jobKey :"+jobKey);
		try {
			schedulerFactoryBean.getScheduler().triggerJob(jKey);
			System.out.println("Job with jobKey :"+jobKey+ " started now succesfully.");
			return true;
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while starting job now with key :"+jobKey+ " message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Check if job is already running
	 */
	public boolean isJobRunning(String jobName) {
		System.out.println("Request received to check if job is running");

		String jobKey = jobName;
		String groupKey = "SampleGroup";

		System.out.println("Parameters received for checking job is running now : jobKey :"+jobKey);
		try {

			List<JobExecutionContext> currentJobs = schedulerFactoryBean.getScheduler().getCurrentlyExecutingJobs();
			if(currentJobs!=null){
				for (JobExecutionContext jobCtx : currentJobs) {
					String jobNameDB = jobCtx.getJobDetail().getKey().getName();
					String groupNameDB = jobCtx.getJobDetail().getKey().getGroup();
					if (jobKey.equalsIgnoreCase(jobNameDB) && groupKey.equalsIgnoreCase(groupNameDB)) {
						return true;
					}
				}
			}
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while checking job with key :"+jobKey+ " is running. error message :"+e.getMessage());
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * Get all jobs
	 */
	public List<Map<String, Object>> getAllJobs() {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			Scheduler scheduler = schedulerFactoryBean.getScheduler();

			for (String groupName : scheduler.getJobGroupNames()) {
				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {

					String jobName = jobKey.getName();
					String jobGroup = jobKey.getGroup();

					JobDetail jobDetails = scheduler.getJobDetail(jobKey);

					System.out.println("@@@@@@@@@@@@@@@ jobDetails : "+ jobDetails);

					//get job's trigger
					List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
					Date scheduleTime = triggers.get(0).getStartTime();
					Date nextFireTime = triggers.get(0).getNextFireTime();
					Date lastFiredTime = triggers.get(0).getPreviousFireTime();

					String jobDescription = jobDetails.getDescription();
					Class<? extends Job> jobClass = jobDetails.getJobClass();
					JobDataMap jobDataMap = jobDetails.getJobDataMap();

					Map<String, Object> map = new HashMap<String, Object>();
					map.put("jobName", jobName);
					map.put("groupName", jobGroup);
					map.put("scheduleTime", scheduleTime);
					map.put("lastFiredTime", lastFiredTime);
					map.put("nextFireTime", nextFireTime);
					map.put("jobDescription", jobDescription);
					map.put("jobClass", jobClass);
					map.put("jobDataMap", jobDataMap);

					if(isJobRunning(jobName)){
						map.put("jobStatus", "RUNNING");
					}else{
						String jobState = getJobState(jobName, jobGroup);
						map.put("jobStatus", jobState);
					}

					/*					Date currentDate = new Date();
					if (scheduleTime.compareTo(currentDate) > 0) {
						map.put("jobStatus", "scheduled");

					} else if (scheduleTime.compareTo(currentDate) < 0) {
						map.put("jobStatus", "Running");

					} else if (scheduleTime.compareTo(currentDate) == 0) {
						map.put("jobStatus", "Running");
					}*/

					list.add(map);
					System.out.println("Job details:");
					System.out.println("Job Name:"+jobName + ", Group Name:"+ groupName + ", Schedule Time:"+scheduleTime);
				}

			}
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while fetching all jobs. error message :"+e.getMessage());
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Check job exist with given name
	 */
	public boolean isJobWithNamePresent(String jobName) {
		try {
			String groupKey = "SampleGroup";
			JobKey jobKey = new JobKey(jobName, groupKey);
			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			if (scheduler.checkExists(jobKey)){
				return true;
			}
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while checking job with name and group exist:"+e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Get the current state of job
	 * @param jobGroup
	 */
	public String getJobState(String jobName, String jobGroup) {
		System.out.println("JobServiceImpl.getJobState()");

		try {
			JobKey jobKey = new JobKey(jobName, jobGroup);

			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			JobDetail jobDetail = scheduler.getJobDetail(jobKey);

			System.out.println("jobDetail() : " + jobDetail);
			System.out.println("jobKey: " + jobKey);


			List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobDetail.getKey());
			if(triggers != null && triggers.size() > 0){
				for (Trigger trigger : triggers) {
					TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());

					if (TriggerState.PAUSED.equals(triggerState)) {
						return "PAUSED";
					}else if (TriggerState.BLOCKED.equals(triggerState)) {
						return "BLOCKED";
					}else if (TriggerState.COMPLETE.equals(triggerState)) {
						return "COMPLETE";
					}else if (TriggerState.ERROR.equals(triggerState)) {
						return "ERROR";
					}else if (TriggerState.NONE.equals(triggerState)) {
						return "NONE";
					}else if (TriggerState.NORMAL.equals(triggerState)) {
						return "SCHEDULED";
					}
				}
			}
		} catch (SchedulerException e) {
			System.out.println("SchedulerException while checking job with name and group exist:"+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Stop a job
	 */
	public boolean stopJob(String jobName) {
		System.out.println("JobServiceImpl.stopJob()");
		try{
			String jobKey = jobName;
			String groupKey = "SampleGroup";

			Scheduler scheduler = schedulerFactoryBean.getScheduler();
			JobKey jkey = new JobKey(jobKey, groupKey);

			return scheduler.interrupt(jkey);

		} catch (SchedulerException e) {
			System.out.println("SchedulerException while stopping job. error message :"+e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	public ScheduledJobDOB getJobDetails(String jobName) throws SchedulerException {

		String groupKey = "SampleGroup";
		JobKey jobKey = new JobKey(jobName , groupKey);

		Scheduler scheduler = schedulerFactoryBean.getScheduler();

		JobDetail jobDetail = scheduler.getJobDetail(jobKey);

		List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
		Date startDate = triggers.get(0).getStartTime();
		Date nextFireTime = triggers.get(0).getNextFireTime();
		Date lastRunTime = triggers.get(0).getPreviousFireTime();

		ScheduledJobDOB schJob = new ScheduledJobDOB();
		String jobStatus = null;

		if(isJobRunning(jobName)){
			jobStatus="RUNNING";
		}else{
			String jobState = getJobState(jobName, groupKey);
			jobStatus=jobState;
		}




		String className = jobDetail.getJobClass().getName();
		String description = jobDetail.getDescription();

		schJob.setName(jobName);
		schJob.setClassName(className);
		schJob.setDescription(description);
		schJob.setJobStatus(jobStatus);
		schJob.setStartDate(startDate);
		schJob.setLastRunTime(lastRunTime);
		schJob.setNextRunTime(nextFireTime);

		return schJob;
	}


}
