package org.idx.scheduler.job;

import java.util.List;
import java.util.Map;

import org.idx.scheduler.service.SchedulerService;
import org.quartz.InterruptableJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.UnableToInterruptJobException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;


public class TestApplicationJob extends QuartzJobBean implements InterruptableJob{
	
	private volatile boolean toStopFlag = true;
	
	@Autowired
	SchedulerService jobService;
	
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		JobKey key = jobExecutionContext.getJobDetail().getKey();
		System.out.println("========= SUNO SUNO SUNO ==================");
		System.out.println("TestApplicationJob started with key :" + key.getName() + ", Group :"+key.getGroup() + " , Thread Name :"+Thread.currentThread().getName());
//		
//		System.out.println("======================================");
//		System.out.println("Accessing annotation example: "+jobService.getAllJobs());
//		List<Map<String, Object>> list = jobService.getAllJobs();
//		System.out.println("Job list :"+list);
//		System.out.println("======================================");
//		
//		//*********** For retrieving stored key-value pairs ***********/
//		JobDataMap dataMap = jobExecutionContext.getMergedJobDataMap();
//		String myValue = dataMap.getString("myKey");
//		System.out.println("Value:" + myValue);
//
// 
//		while(toStopFlag){
//			try {
//				System.out.println("Test Job Running... Thread Name :"+Thread.currentThread().getName());
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
		System.out.println("Thread: "+ Thread.currentThread().getName() +" stopped.");
	}

	public void interrupt() throws UnableToInterruptJobException {
		System.out.println("Stopping thread... ");
		toStopFlag = false;
	}
}