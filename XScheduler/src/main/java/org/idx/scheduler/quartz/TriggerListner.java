package org.idx.scheduler.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.TriggerListener;
import org.springframework.stereotype.Component;

@Component
public class TriggerListner implements TriggerListener {

    public String getName() {
        return "globalTrigger";
    }

    public void triggerFired(Trigger trigger, JobExecutionContext context) {
    	System.out.println("TriggerListner.triggerFired()");
    }

    public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
    	System.out.println("TriggerListner.vetoJobExecution()");
        return false;
    }

    public void triggerMisfired(Trigger trigger) {
    	System.out.println("TriggerListner.triggerMisfired()");
        String jobName = trigger.getJobKey().getName();
        System.out.println("Job name: " + jobName + " is misfired");
        
    }

    public void triggerComplete(Trigger trigger, JobExecutionContext context, CompletedExecutionInstruction triggerInstructionCode) {
        System.out.println("TriggerListner.triggerComplete()");
    }
}

